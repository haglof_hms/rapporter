use [Sodra Navig Test]

SELECT b1.tdata_trakt_id,b1.tdata_spc_id,b1.tdata_spc_name,round([tdata_percent],1) as Trdslgperc,b2.*,b3.*
FROM [Sodra Navig Test].[dbo].[esti_trakt_data_table] 
as b1 
left outer join (
  SELECT b2.tdata_trakt_id,b2.tdata_spc_id,b2.tdata_spc_name,b2.tdata_data_type,round(b2.tdata_percent,1)  as Trdslgperc
  FROM [Sodra Navig Test].[dbo].[esti_trakt_data_table] as b2
  )as b2 on b1.tdata_trakt_id=b2.tdata_trakt_id
left outer join (
  SELECT b3.tdata_trakt_id,b3.tdata_spc_id,b3.tdata_spc_name,b3.tdata_data_type,round(b3.tdata_percent,1)  as Trdslgperc
  FROM [Sodra Navig Test].[dbo].[esti_trakt_data_table] as b3
  )as b3 on b1.tdata_trakt_id=b3.tdata_trakt_id
left outer join (
  SELECT b4.tdata_trakt_id,b4.tdata_spc_id,b4.tdata_spc_name,b4.tdata_data_type,round(b4.tdata_percent,1)  as Trdslgperc
  FROM [Sodra Navig Test].[dbo].[esti_trakt_data_table] as b4
  )as b4 on b3.tdata_trakt_id=b4.tdata_trakt_id
  
where b1.tdata_data_type=1 and b1.tdata_spc_id=1 
and b2.tdata_data_type=1 and b2.tdata_spc_id=2  
and b3.tdata_data_type=1 and b3.tdata_spc_id=3  
and b4.tdata_data_type=1 and b4.tdata_spc_id=9  




