

  SELECT cast(a4.trakt_created_by as varchar) ,a4.trakt_id, a3.*    
  FROM esti_trakt_table 
  AS a4 
  INNER JOIN
  (                
  	SELECT id,trakt_sodra_avdnr,trakt_sodra_medelhojd,trakt_sodra_volym,trakt_sodra_standortsi,trakt_sodra_gryndyta,
	trakt_sodra_dgv,trakt_created_by as trakt_sodra_created_by, a_1.* 
	FROM esti_trakt_sodra_gis_table 
	AS a2 
	INNER JOIN
		(
		SELECT  a.tdcls_trakt_id, SQRT(SUM((((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0) * ((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0))
		* (a.tdcls_numof + a.tdcls_numof_randtrees)) / SUM(a.tdcls_numof + a.tdcls_numof_randtrees)) AS fDG, SUM(((((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0)
		* ((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0)) * ((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0)) * (a.tdcls_numof + a.tdcls_numof_randtrees))
		/ SUM((((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0) * ((a.tdcls_dcls_to + a.tdcls_dcls_from) / 2.0)) * (a.tdcls_numof + a.tdcls_numof_randtrees)) AS fDGV
		FROM esti_trakt_dcls_trees_table AS a 
		GROUP BY a.tdcls_trakt_id 
		) AS a_1 ON a2.id = a_1.tdcls_trakt_id 
   ) AS a3 ON a4.trakt_id = a3.id  
  WHERE a4.trakt_created_by='JONORN-6' or a4.trakt_created_by='KALLE'

