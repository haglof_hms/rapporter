use [Sodra Navig Test]


SELECT b1.tdata_trakt_id, 
round(b1.tdata_percent,1) as TallTrf, 
round(b2.tdata_percent,1) GranTrf, 
round(b3.tdata_percent,1) as Bj�rkTrf, 
round(b4.tdata_percent,1) as AspTrf, 
round(b5.tdata_percent,1) as EkTrf,
round(b6.tdata_percent,1) as BokTrf
FROM esti_trakt_data_table as b1
	LEFT OUTER JOIN esti_trakt_data_table b2
	ON b2.tdata_trakt_id = b1.tdata_trakt_id AND b2.tdata_data_type=1 and b2.tdata_spc_id=2
	LEFT OUTER JOIN esti_trakt_data_table b3
	ON b3.tdata_trakt_id = b1.tdata_trakt_id AND b3.tdata_data_type=1 and b3.tdata_spc_id=3 
	LEFT OUTER JOIN esti_trakt_data_table b4
	ON b4.tdata_trakt_id = b1.tdata_trakt_id AND b4.tdata_data_type=1 and b4.tdata_spc_id=23
	LEFT OUTER JOIN esti_trakt_data_table b5
	ON b5.tdata_trakt_id = b1.tdata_trakt_id AND b5.tdata_data_type=1 and b5.tdata_spc_id=9
	LEFT OUTER JOIN esti_trakt_data_table b6
	ON b6.tdata_trakt_id = b1.tdata_trakt_id AND b6.tdata_data_type=1 and b6.tdata_spc_id=13
WHERE b1.tdata_data_type=1 and b1.tdata_spc_id=1 
